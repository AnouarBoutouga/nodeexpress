const express = require("express");
const fs = require("fs");

const app = express();
app.use(express.json());

app.get("/users", (req, res) => {
    let users = fs.readFileSync("./data/users.json", "utf8");
    users = JSON.parse(users);
    res.json(users);
});

app.post("/users", (req, res) => {
    let users = fs.readFileSync("./data/users.json", "utf8");
    users = JSON.parse(users);
    users.push(req.body);
    fs.writeFileSync("./data/users.json", JSON.stringify(users));
    res.send({
        status: true,
        message: `L'utilisateur ${req.body.name} à été créé avec success!`
    });
});

app.put("/users", (req, res) => {
    let users = fs.readFileSync("./data/users.json", "utf8");
    users = JSON.parse(users);
    const index = users.findIndex(item => item.id == req.body.id);
    users.splice(index, 1, req.body);
    fs.writeFileSync("./data/users.json", JSON.stringify(users));
    res.send({
        status: true,
        message: `L'utilisateur ${req.body.name} à été modifié avec success!`
    });
});

app.listen(3000, () => {
    console.log("App running on port 3000");
});
